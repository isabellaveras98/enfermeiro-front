FROM node:13.6.0 as builder

WORKDIR /var/lib/app/
COPY ./package.json ./
COPY ./.env ./
COPY ./yarn.lock ./
COPY ./src/ ./src
COPY ./public/ ./public

ARG PUBLIC_URL
RUN curl -o- -L https://yarnpkg.com/install.sh --version 0.26.1
RUN yarn install --production --silent --non-interactive
ENV PATH /var/lib/app/node_modules/.bin:$PATH
RUN yarn run build

FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /var/lib/app/build/ /var/www/