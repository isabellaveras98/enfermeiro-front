## Admin UI

![license](https://img.shields.io/badge/license-MIT-blue.svg)

> User interface for scheduling administration. This project was cloned from React Material Dashboard and can be found [here](https://github.com/devias-io/react-material-dashboard).

## Quick start

- Make sure your NodeJS and npm versions are up to date for `React 16.8.6`

- Install dependencies: `npm install` or `yarn`

- Start the server: `npm run start` or `yarn start`

- Views are on: `localhost:3001`

## Documentation

The documentation for the React Material Kit is can be found [here](https://material-ui.com?ref=devias-io).

## File Structure

Within the download you'll find the following directories and files:

```
material-react-dashboard

├── .eslintrc
├── .gitignore
├── .prettierrc
├── CHANGELOG.md
├── jsconfig.json
├── LICENSE.md
├── package.json
├── README.md
├── public
├── docs
└── src
	├── assets
	├── common
	├── components
	├── helpers
	├── icons
	├── layouts
	├── theme
	├── views
	│	├── Account
	│	├── Dashboard
	│	├── Icons
	│	├── NotFound
	│	├── ProductList
	│	├── Settings
	│	├── FindPatient
	│	├── SignUp
	│	├── Typography
	│	└── UserList
	├── App.jsx
	├── index.jsx
	└── Routes.jsx
```

## License

- Licensed under MIT (https://github.com/devias-io/react-material-dashboard/blob/master/LICENSE.md)


