/* eslint-disable react/jsx-max-props-per-line */
/* eslint-disable no-undef */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import { Chart } from 'react-chartjs-2';
import { ThemeProvider } from '@material-ui/styles';
import validate from 'validate.js';
import axios from 'axios';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

import { chartjs } from './helpers';
import theme from './theme';
import 'react-perfect-scrollbar/dist/css/styles.css';
import './assets/scss/index.scss';
import validators from './common/validators';
import { Types as VaccineTypes } from 'sagas/vaccine';
import Routes from './Routes';

import storageHelper from 'helpers/storage';
import { localeMap, localeUtilsMap } from 'utils/LocaleUtils';
import Nav from 'layouts/Nav/Nav';

const locale = 'pt';
const history = './utils/history';

Chart.helpers.extend(Chart.elements.Rectangle.prototype, {
  draw: chartjs.draw
});

if (process.env.NODE_ENV === 'development') {
  axios.defaults.baseURL = 'http://localhost:8080/enfermeiro';
}
validate.validators = {
  ...validate.validators,
  ...validators
};

function App() {
  const dispatch = useDispatch();
  const id = useSelector((state) => state.nurse.id);

  useEffect(() => {
    async function validateToken() {
      const storage = storageHelper.get();

      if (!id || id === null) {
        dispatch({ type: 'FAILED_AUTHENTICATION' });
        return;
      }
      dispatch({
        type: VaccineTypes.findAllActive,
        payload: {
          data: {
            id: id
          }
        },
        onSuccess: () => {
          
        }
      });
    }
    validateToken();
  }, [id]);

  return (
    <ThemeProvider theme={theme}>
      <MuiPickersUtilsProvider
        locale={localeMap[locale]}
        utils={localeUtilsMap[locale]}
      >
        <Router basename={process.env.PUBLIC_URL} history={history}>
          <Nav />
          <Routes />
        </Router>
      </MuiPickersUtilsProvider>
    </ThemeProvider>
  );
}

export default App;
export { locale };
