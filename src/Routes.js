import Content from 'layouts/Content/Content';
import React from 'react';
import { useSelector } from 'react-redux';
import { Switch, Redirect, Route } from 'react-router-dom';

import { RouteWithLayout } from './components';
import { Minimal as MinimalLayout } from './layouts';

import {
  NotFound as NotFoundView,
  FindPatient as FindPatientView,
  Patient as PatientView,
  Profile as ProfileView,
  AllVaccines as AllVaccinesView,
  AddNewVaccine as AddNewVaccineView,
  Login as LoginView,
  VaccinationForm as VaccinationFormView,
  Unauthorized as UnauthorizedView,
} from './views';

const Routes = () => {

  return (
    <Switch>
      <Redirect
        exact
        from="/"
        to="/login"
      />
      <Route path="/find-patient">
        <FindPatientView />
        <Content title="Buscar Paciente" />
      </Route>
      <Route path="/login">
        <LoginView />
        <Content title="Login" />
      </Route>
      <Route path="/vaccination/form">
        <VaccinationFormView />
        <Content title="Nova Vacinação" />
      </Route>
      <Route path="/patient"> 
        <PatientView />
      </Route>
      <Route path="/add-vaccine">
        <AddNewVaccineView />
        <Content title="Nova Vacina" />
      </Route>
      <Route path="/profile">
        <ProfileView />
        <Content title="Perfil" />
      </Route>
      <Route path="/all-vaccines">
        <AllVaccinesView />
        <Content title="Todas as vacinas" />
      </Route>
      <RouteWithLayout
        component={NotFoundView}
        exact
        layout={MinimalLayout}
        path="/not-found"
      />
      <RouteWithLayout
        component={UnauthorizedView}
        exact
        layout={MinimalLayout}
        path="/unauthorized"
      />
      <Redirect to="/not-found" />
    </Switch>
  );
};

export default Routes;
