import moment from 'moment';

export default {
  parse: (value) => {
    const date = moment(value, 'YYYY-MM-DD');
    const d = new Date(date.year(), date.month(), date.date());

    return d;
  }
}
