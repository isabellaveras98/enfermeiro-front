export default [
  { value: 'ALL', text: '(Todas)' },
  { value: 'CLASSIC', text: 'CLASSIC' },
  { value: 'ESTACAO X', text: 'ESTACAO X' },
  { value: 'EXCLUSIVE', text: 'EXCLUSIVE' },
  { value: 'PRIME', text: 'PRIME' },
];
