import timezone from 'helpers/timezone';
import moment from 'moment';

const timezoned = {
  format: (value) => {
    return moment(value).add('minutes', timezone.uffset).format('DD/MM/YYYY HH:mm:ss');
  }
}

const format = (value, pattern) => {
  if (pattern && pattern.length > 0) {
    return moment(value).format(pattern);
  }
  return moment(value).format('DD/MM/YYYY HH:mm:ss');
}

export default {
  format,
  timezoned: timezoned,
}
