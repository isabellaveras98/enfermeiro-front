import ExportJsonExcel from 'js-export-excel';

export default ( data, headers, sheetName, title ) => {
  const option = {
    datas: [{
      sheetData: data,
      sheetHeader: headers,
      sheetName: sheetName,
    }],
    fileName: title,
  };

  const exporter = new ExportJsonExcel(option);
  exporter.saveExcel();
}
