import jsPDF from 'jspdf';
import 'jspdf-autotable';

export default (data, fillColor = '#fff', filename, headers, title) => {
  if (!data || !Array.isArray(data) || data.length === 0) {
    return;
  }

  const unit = 'pt';
  const size = 'A4';
  const orientation = 'landscape'; // portrait or landscape

  const marginLeft = 40;
  const doc = new jsPDF(orientation, unit, size);

  let content = {
    startY: 50,
    head: headers,
    body: data,
    headStyles: { fillColor: fillColor },
  };

  doc.setFontSize(15);
  doc.text(title, marginLeft, 40);
  doc.autoTable(content);
  doc.save(title + '.pdf');
}
