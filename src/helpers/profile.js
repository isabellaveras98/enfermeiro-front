const profiles = [
  { id: 0, name: 'Administrador', description: 'Permite administrar todos recursos do sistema' },
  { id: 1, name: 'Diretor', description: 'Permite visualizar algumas configurações, acessar a plataforma de entrevista e o dashboard dos projetos' },
  { id: 2, name: 'Gerente', description: 'Permite acessar apenas a plataforma de entrevista' },
  { id: 3, name: 'Regional', description: 'Permite acessar a plataforma de entrevista e o dashboard dos projetos, apenas das agencias que a sua reagional' },
];

function getProfile(id) {
  const founds = profiles.filter((p) => p.id === Number.parseInt(id, 10));

  if (!founds || founds.length === 0) {
    return undefined;
  }

  return founds[0];
}

export default {
  list: profiles,
  getName: (id) => {
    const profile = getProfile(id);

    if (!profile) {
      return 'Indefinido';
    }

    return profile.name;
  },
  get: (id) => getProfile(id),
}
