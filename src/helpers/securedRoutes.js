// 0 = Administrador
// 1 = Gerente
// 2 = Consignado
// 3 = Coordenador

const routes = [{
  path: '/home',
  profiles: [0, 1]
}, {
  path: '/projects',
  profiles: [0]
}, {
  path: '/projects/form',
  profiles: [0]
}, {
  path: '/customers',
  profiles: [0]
}, {
  path: '/users',
  profiles: [0]
}, {
  path: '/users/upload',
  profiles: [0]
}, {
  path: '/users/form',
  profiles: [0]
}, {
  path: '/upload-file',
  profiles: [0]
}, {
  path: '/find-patient',
  profiles: [0]
}, {
  path: '/login',
  profiles: [1]
}, {
  path: '/vaccination/form',
  profiles: [1]
}, {
  path: '/patient',
  profiles: [1]
}, {
  path: '/profile',
  profiles: [1]
},
]

const getProfiles = (path) => {
  const route = routes.filter((route) => route.path === path);
  if (route.length === 0) { return [] }

  return route[0].profiles;
}

export { getProfiles };
