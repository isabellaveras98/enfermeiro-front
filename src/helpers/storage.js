/* eslint-disable no-undef */

const defaultKeyName = 'bra-flog-admin';

const getStorageKey = () => localStorage.getItem(process.env.REACT_APP_LOCAL_STORAGE_KEY || defaultKeyName);

const get = () => {
  const storage = getStorageKey();

  if (null !== storage) {
    return JSON.parse(storage);
  }

  return null;
};

const set = (data) => {
  localStorage.setItem(defaultKeyName, JSON.stringify(data));
};

const addValue = (key, value) => {
  let storage = get();

  if (storage === null) {
    storage = {};
  }

  storage[key] = value;

  set(storage);
};

const removeKey = (key) => {
  if (!['project', 'storage'].includes(key)) {
    console.error('Invalid key to remove on storage helper');
    return;
  }
  const storage = get();
  delete storage[key];

  set(storage);
};

export default {
  get,
  set,
  addValue,
  removeKey,
}
