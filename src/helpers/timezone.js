import moment from 'moment-timezone';

const zone = moment.tz.guess(true);
const m = moment();

export default {
  uffset: m.tz(zone).utcOffset()
};
