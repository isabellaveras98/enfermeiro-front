import { AppBar, Slide, Toolbar, Typography, useScrollTrigger } from '@material-ui/core';
import React from 'react'

import './Content.css';


const Content = props => {
    function HideOnScroll(props) {
        const { children, window } = props;
        const trigger = useScrollTrigger({ target: window ? window() : undefined });
        return (
            <Slide appear={false} direction="down" in={!trigger}>
                {children}
            </Slide>
        );
    }

    return (

        <div className="content">
            <HideOnScroll {...props}>
                <div className="app-bar">
                    <AppBar>
                        <Toolbar>
                            <Typography variant="h6" className="toolbar">
                                {props.title}
                            </Typography>
                        </Toolbar>
                    </AppBar>
                </div>
            </HideOnScroll>

        </div>

    )
}

export default Content