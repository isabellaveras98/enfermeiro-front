import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import MenuIcon from '@material-ui/icons/Menu';
import { ExitToApp, Person, Search, Add,List } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import './Nav.css'
import { If } from 'helpers';
import { useDispatch, useSelector } from 'react-redux';
import { Types as NurseTypes } from 'sagas/nurse';


export default function Nav(props) {
    const [anchorEl, setAnchorEl] = React.useState(null);

    const { history } = props;

    const dispatch = useDispatch();

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    const logof = () => {
        dispatch({
            type: NurseTypes.logout,
            payload: {
                onSuccess: () => {
                    history.push('/login');
                }
            }
        }
        )
    };
    const open = Boolean(anchorEl);
    const nurse = useSelector((state) => state.nurse);
    return (
        <div className="menu-bar">
            <div>
                <If test={nurse.nome !== ''}>
                    <Button
                        id="basic-button"
                        aria-controls="basic-menu"
                        aria-haspopup="true"
                        aria-expanded={open ? 'true' : undefined}
                        onClick={handleClick}
                    >
                        <MenuIcon />
                    </Button>
                    <Menu
                        id="basic-menu"
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        MenuListProps={{
                            'aria-labelledby': 'basic-button',
                        }}
                    >
                        <MenuItem onClick={handleClose}>
                            <Link to='find-patient'>
                                <ListItemIcon>
                                    <Search />
                                </ListItemIcon>
                                Buscar Paciente
                            </Link>
                        </MenuItem>
                        <MenuItem >
                            <Link to='/profile' >
                                <ListItemIcon>
                                    <Person />
                                </ListItemIcon>

                                Perfil
                            </Link>
                        </MenuItem>
                        <MenuItem >
                            <Link to='/add-vaccine' >
                                <ListItemIcon>
                                    <Add />
                                </ListItemIcon>

                                Adicionar Nova Vacina
                            </Link>
                        </MenuItem>
                        <MenuItem >
                            <Link to='/all-vaccines' >
                                <ListItemIcon>
                                    <List />
                                </ListItemIcon>

                                Todas as Vacinas
                            </Link>
                        </MenuItem>
                        <MenuItem onClick={logof}>
                            <ListItemIcon>
                                <ExitToApp />
                            </ListItemIcon>
                            Sair
                        </MenuItem>



                    </Menu>
                </If>
            </div>
        </div>
    );
}