/* eslint-disable no-undef */
import { call, put } from 'redux-saga/effects';
import nurseService from 'services/nurse';



function* requestLogin(action) {
  try {
    const response = yield call(nurseService.login, action.payload.data);

    if (action.payload && action.payload.onSuccess) {
      yield put({ type: 'SUCCESS_NURSE_LOGIN', payload: response.data });

    } else {
      console.error('Invalid patients data: ' + response.data);
    }

    if (action && action.payload && action.payload.onSuccess) {
      action.payload.onSuccess(response.data);
    }
  } catch (error) {
    yield put({ type: 'FAILED_NURSE_LOGIN' });

    if (action.payload && action.payload.onError) {
      action.payload.onError(error);
    }
  }
}

function* requestLogout(action) {
  try {
    yield put({ type: 'DEFINE_NURSE_DEFAULT' });

    if (action.payload && action.payload.onSuccess) {
      window.location.href = "/login";
      action.payload.onSuccess();
      
    }
  } catch (error) {
    if (action.payload && action.payload.onError) {
      action.payload.onError(error);
    }
  }
}



const Actions = {
  requestLogin,
  requestLogout,
}

const Types = {
  login: 'REQUEST_NURSE_LOGIN',
  logout: 'REQUEST_NURSE_LOGOUT',
}

export {
  Actions,
  Types,
}
