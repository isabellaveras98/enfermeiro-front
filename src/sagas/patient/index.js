/* eslint-disable no-undef */
import { call, put } from 'redux-saga/effects';
import patientService from 'services/patient';



function* requestFind(action) {
  try {
    const response = yield call(patientService.find, action.payload.data.cns);

    if (action.payload && action.payload.onSuccess) {
      yield put({ type: 'SUCCESS_FIND_PATIENT', payload: response.data });

    } else {
      console.error('Invalid patients data: ' + response.data);
    }

    if (action && action.payload && action.payload.onSuccess) {
      action.payload.onSuccess(response.data);
    }
  } catch (error) {
    yield put({ type: 'FAILED_FIND_PATIENT' });

    if (action.payload && action.payload.onError) {
      action.payload.onError(error);
    }
  }
}



const Actions = {
  requestFind,
}

const Types = {
  find: 'REQUEST_PATIENT_FIND',
}

export {
  Actions,
  Types,
}
