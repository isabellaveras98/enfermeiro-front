import { put } from 'redux-saga/effects';

function* requestClose() {
  yield put({ type: 'CLOSE_SNACKBAR' });
}

function* requestShow(action) {
  yield put({ type: 'SHOW_SNACKBAR', payload: action.payload });
}

const Actions = {
  requestClose,
  requestShow,
}

const Types = {
  close: 'REQUEST_CLOSE_SNACKBAR',
  show: 'REQUEST_SHOW_SNACKBAR',
}

export {
  Actions,
  Types,
}
