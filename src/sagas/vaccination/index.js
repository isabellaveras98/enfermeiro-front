import { put, call } from 'redux-saga/effects';
import patientService from 'services/patient';


function* requestAddVaccinationForm(action) {
    try {
        yield put({ type: 'DEFINE_DEFAULT_VACCINATION_FORM_DATA' });

        if (action.payload && action.payload.onSuccess) {
            action.payload.onSuccess();
        }
    } catch (error) {
        console.error(error);

        if (action.payload && action.payload.onError) {
            action.payload.onError(error);
        }
    }
}

function* requestSaveVaccination(action) {
    try {
        
        yield call(patientService.save, action.payload.data);

        const response = yield call(patientService.find, action.payload.data.cns);


        if (action.payload && action.payload.onSuccess) {
      yield put({ type: 'SUCCESS_FIND_PATIENT', payload: response.data });

    } else {
      console.error('Invalid patients data: ' + response.data);
    }

    } catch (error) {
        if (action.payload && action.payload.onError) {
            action.payload.onError(error);
        }
    }
}

const Actions = {
    requestAddVaccinationForm,
    requestSaveVaccination
}

const Types = {
    addVaccinationForm: 'REQUEST_ADD_VACCINATION_FORM',
    saveVaccination: 'REQUEST_ADD_VACCINATION_FORM',
}

export {
    Actions,
    Types,
}