import { call, put } from 'redux-saga/effects';
import vaccineService from 'services/vaccines';


function* requestFind(action) {
  try {
    const response = yield call(vaccineService.find, action.payload.data);

    if (action.payload && action.payload.onSuccess) {
      yield put({ type: 'SUCCESS_FIND_VACCINE', payload: response.data });

    } else {
      console.error('Invalid vaccines data: ' + response.data);
    }

    if (action && action.payload && action.payload.onSuccess) {
      action.payload.onSuccess(response.data);
    }
  } catch (error) {
    yield put({ type: 'FAILED_FIND_VACCINE' });

    if (action.payload && action.payload.onError) {
      action.payload.onError(error);
    }
  }
}
function* requestFindAllActive(action) {
  try {
    const response = yield call(vaccineService.findAll, action.payload.data.id);

    if (Array.isArray(response.data)) {
      yield put({ type: 'SUCCESS_VACCINE_FIND_ALL', payload: response.data });

    } else {
      console.error('Invalid vaccines data: ' + response.data);
    }

    if (action && action.payload && action.payload.onSuccess) {
      action.payload.onSuccess(response.data);
    }
  } catch (error) {
    yield put({ type: 'FAILED_FIND_ALL_VACCINE' });

    if (action.payload && action.payload.onError) {
      action.payload.onError(error);
    }
  }
}
function* requestDelete(action) {
  try {

    yield call(vaccineService.delete, action.payload.idVacina);
    const response = yield call(vaccineService.findAll, action.payload.id);

    if (action.payload && action.payload.onSuccess) {
      yield put({ type: 'SUCCESS_VACCINE_DELETE' });
      yield put({ type: 'SUCCESS_VACCINE_FIND_ALL', payload: response.data });


    } else {
      console.error('Invalid vaccines data: ');
    }

       
  } catch (error) {
    yield put({ type: 'FAILED_VACCINE_DELETE' });

    if (action.payload && action.payload.onError) {
      action.payload.onError(error);
    }
  }
}
function* requestAddVaccine(action) {

  try {
    const response = yield call(vaccineService.add, action.payload.data);
    const allVaccines = yield call(vaccineService.findAll, action.payload.data.idUbs);

    if (action.payload && action.payload.onSuccess) {
      yield put({ type: 'SUCCESS_ADD_VACCINE', payload: response.data });
      yield put({ type: 'SUCCESS_VACCINE_FIND_ALL', payload: allVaccines.data });


    } else {
      console.error('Invalid vaccines data: ' + response.data);
    }

    if (action && action.payload && action.payload.onSuccess) {
      action.payload.onSuccess(response.data);
    }
  } catch (error) {
    yield put({ type: 'FAILED_FIND_VACCINE' });

    if (action.payload && action.payload.onError) {
      action.payload.onError(error);
    }
  }
}


const Actions = {
  requestFind,
  requestAddVaccine,
  requestFindAllActive,
  requestDelete,
}

const Types = {
  find: 'REQUEST_VACCINE_FIND',
  add: 'REQUEST_VACCINE_ADD',
  findAllActive: 'REQUEST_VACCINE_FIND_ALL',
  delete: 'REQUEST_VACCINE_DELETE',
}

export {
  Actions,
  Types,
}
