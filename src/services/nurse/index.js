import axios from 'axios';

export default {
  login: async (data) => {
    return axios.get('/enfermeiros/login', {
      headers: {
        senha: data.password,
        login: data.login
      }
    });
  }
}
