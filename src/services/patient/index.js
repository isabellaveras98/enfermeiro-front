import axios from 'axios';

export default {
  find: async (cns) => {
    return axios.get('/pacientes/buscar', {
      headers: {
        cns: cns,
      }
    });
  },
  save: async (data) => {
    return axios.post('/pacientes/vacinar', {}, {
      headers: {
        lote: data.lote,
        nome: data.nome,
        cns: data.cns,
        idEnfermeiro: data.id
      }
    });
  },

}
