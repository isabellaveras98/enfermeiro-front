import axios from 'axios';

export default {
  find: async (data) => {
    return axios.get('/vacinas/dadosVacina',{ headers: {
      nome: data.nome,
      lote: data.lote,
    } });
  },
  findAll: async (data) => {
    return axios.get('/vacinas/todas',{ headers: {
      id:data,
    } });
  },
  delete: async (data) => {
    return axios.post('/vacinas/delete',{},{ headers: {
      id: data,
    } });
  },
  add: async (data) => {
    return axios.post('/vacinas/save',{},{ headers: {
      nome: data.nome,
      lote: data.lote,
      dtValidade:data.dtValidade,
      idUbs:data.idUbs,
      fornecedor:data.fornecedor,
      nomeCampanha:data.nomeCampanha,
      cor:data.cor.hex
    } });
  },
}
