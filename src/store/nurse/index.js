const initialState = {
  nome:'',
  profile:1
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SUCCESS_NURSE_LOGIN':
      return { ...state, ...action.payload };
      case 'DEFINE_NURSE_DEFAULT':
        return { ...initialState };
    default:
      return state;
  }
}
