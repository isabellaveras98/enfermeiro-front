const initialState = {};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SUCCESS_FIND_PATIENT':
      return { ...state, ...action.payload };
    default:
      return state;
  }
}
