import { combineReducers } from 'redux';

import snackbar from './snackbar';
import patient from './patient';
import nurse from './nurse';
import vaccinationForm from './vaccinationForm';
import vaccine from './vaccine';
import vaccines from './vaccines';


export default combineReducers({
  snackbar,
  nurse,
  patient,
  vaccine,
  vaccines,
  vaccinationForm,
});
