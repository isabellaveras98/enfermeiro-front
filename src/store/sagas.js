/* eslint-disable no-undef */
import { all, call, takeLatest, put, select, take } from 'redux-saga/effects';

// import storageHelper from 'helpers/storage';

// import { Actions as UserActions, Types as UserTypes } from 'sagas/user';
// import { Actions as CustomerActions, Types as CustomerTypes } from 'sagas/customers';
// import { Actions as BoardActions, Types as BoardTypes } from 'sagas/board';
// import { Actions as ProjectActions, Types as ProjectTypes } from 'sagas/projects';
import { Actions as PatientActions, Types as PatientTypes } from 'sagas/patient';
import { Actions as NurseActions, Types as NurseTypes } from 'sagas/nurse';
// import { Actions as UploadFileActions, Types as UploadFileTypes } from 'sagas/uploadFile';
import { Actions as VaccinationActions, Types as VaccinationTypes } from 'sagas/vaccination';
import { Actions as VaccineActions, Types as VaccineTypes } from 'sagas/vaccine';
import { Actions as SnackbarActions, Types as SnackbarTypes } from 'sagas/snackbar';

// function* watchSuccessSelfData() {
//   while (true) {
//     yield take('SUCCESS_SELF_DATA');

//     const authentication = yield select((state) => state.authentication);
//     const storage = storageHelper.get();

//     if (authentication.status === 'success' && (!storage.project || !storage.project.id || !storage.project.id.length > 0)) {
//       yield put({ type: 'SHOW_MENU', payload: {
//         hasProject: false,
//         profile: authentication.user.profile }
//       });
//     }

//     yield call(ProjectActions.requestFindAllActive);
//   }
// }

// function* watchSelectProject() {
//   while (true) {
//     // Ouvir/espera pela tarefa
//     yield take('SUCCESS_SELECT_REGIONAL');

//     // Coletar dados da store
//     const authentication = yield select((state) => state.authentication);
//     const project = yield select((state) => state.project);

//     // Exibir menus
//     yield put({
//       type: 'SHOW_MENU',
//       payload: {
//         hasProject: project.id && project.id.length > 0,
//         profile: authentication.user.profile
//       }
//     });
//   }
// }

// function* watchRemoveProject() {
//   while (true) {
//     // Ouvir/esperar pela tarefa
//     yield take('REMOVE_SELECTED_PROJECT');

//     // Coletar dados da store
//     const authentication = yield select((state) => state.authentication);

//     // Reexibir menu
//     yield put({
//       type: 'SHOW_MENU',
//       payload: {
//         hasProject: false,
//         profile: authentication.user.profile
//       }
//     });

//     // Zerar projeto
//     yield put({ type: 'REMOVE_SELECTED_PROJECT' });
//   }
// }

export default function* root() {
  yield all([
    takeLatest(VaccinationTypes.addVaccinationForm, VaccinationActions.requestAddVaccinationForm),
    takeLatest(VaccineTypes.find, VaccineActions.requestFind),
    takeLatest(VaccineTypes.add, VaccineActions.requestAddVaccine),
    takeLatest(VaccineTypes.delete, VaccineActions.requestDelete),
    takeLatest(VaccineTypes.add, VaccineActions.requestAddVaccine),
    takeLatest(VaccineTypes.findAllActive, VaccineActions.requestFindAllActive),
    takeLatest(VaccinationTypes.saveVaccination, VaccinationActions.requestSaveVaccination),
    takeLatest(PatientTypes.find, PatientActions.requestFind),
    takeLatest(NurseTypes.login, NurseActions.requestLogin),
    takeLatest(NurseTypes.logout, NurseActions.requestLogout),
    // takeLatest(CustomerTypes.findCountCustomerCategory, CustomerActions.requestFindCountCustomerCategory),
    // takeLatest(CustomerTypes.upload, CustomerActions.requestCustomerUploadFile),
    // takeLatest(UserTypes.selfData, UserActions.requestSelfData),
    // takeLatest(UserTypes.authentication, UserActions.requestAuthentication),
    // takeLatest(UserTypes.validateToken, UserActions.requestValidateToken),
    // takeLatest(UserTypes.logout, UserActions.requestLogout),
    // takeLatest(UserTypes.findAllActive, UserActions.requestFindAllActive),
    // takeLatest(UserTypes.findUsersByProfile, UserActions.requestFindUsersByProfile),
    // takeLatest(UserTypes.addUserForm, UserActions.requestAddUserForm),
    // takeLatest(UserTypes.save, UserActions.requestSaveUser),
    // takeLatest(UserTypes.remove, UserActions.requestRemoveUsers),
    // takeLatest(UserTypes.upload, UserActions.requestUsersUploadFile),
    // takeLatest(ProjectTypes.addProjectForm, ProjectActions.requestAddProjectForm),
    // takeLatest(ProjectTypes.saveProject, ProjectActions.requestSaveProject),
    // takeLatest(ProjectTypes.remove, ProjectActions.requestRemoveProjects),
    // takeLatest(ProjectTypes.findAllActive, ProjectActions.requestFindAllActive),
    // takeLatest(ProjectTypes.select, ProjectActions.requestSelectProject),
    // takeLatest(CustomerTypes.findCountCustomerCategory, CustomerActions.requestFindCountCustomerCategory),
    // takeLatest(CustomerTypes.upload, CustomerActions.requestCustomerUploadFile),
    // takeLatest(BoardTypes.findAllActive, BoardActions.requestFindAllActive),
    takeLatest(SnackbarTypes.close, SnackbarActions.requestClose),
    takeLatest(SnackbarTypes.show, SnackbarActions.requestShow),
    // takeLatest(UploadFileTypes.upload, UploadFileActions.requestBankOfficeUploadFile),
    // watchSuccessSelfData(),
    // watchSelectProject(),
    // watchRemoveProject(),
  ]);
}
