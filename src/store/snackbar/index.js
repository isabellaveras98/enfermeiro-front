const initialState = {
  message: '',
  show: false,
  detailList: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'SHOW_SNACKBAR':
      return { ...state, show: true, ...action.payload };
    case 'CLOSE_SNACKBAR':
      return { ...state, message: '', detailList: [], show: false };
    default:
      return state;
  }
}
