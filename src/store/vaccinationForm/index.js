const defaultData = {
    data: {
    },
    title: 'Nova Vacinação',
    subtitle: 'Preencha corretamente lote e o nome da vacina'
  };
  
  const initialState = { ...defaultData };
  
  export default (state = initialState, action) => {
    switch (action.type) {
      case 'SUCCESS_ADD_VACCINATION_DATA':
        return { ...state, data: action.payload };
      case 'DEFINE_DEFAULT_VACCINATION_FORM_DATA':
        return { ...defaultData };
      default:
        return state;
    }
  }