const defaultData = { };
  
const initialState = { ...defaultData };
  
export default (state = initialState, action) => {
  switch (action.type) {
    case 'SUCCESS_FIND_VACCINE_DATA':
      return { ...state, data: action.payload };
    case 'SUCCESS_ADD_VACCINE':
      return { ...state, data: action.payload };
    case 'DEFINE_DEFAULT_VACCINE_DATA':
      return { ...defaultData };
    default:
      return state;
  }
}