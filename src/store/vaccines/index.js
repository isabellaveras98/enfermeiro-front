const defaultData = [];
  
  
  export default (state = defaultData, action) => {
    switch (action.type) {
      case 'SUCCESS_VACCINE_FIND_ALL':
        return { ...state, data: action.payload };
      // case 'SUCCESS_VACCINE_DELETE':
      //   return { ...state, data: action.payload };
      case 'DEFINE_DEFAULT_VACCINE_DATA':
        return { ...defaultData };
      default:
        return state;
    }
  }