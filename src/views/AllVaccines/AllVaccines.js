/* eslint-disable react/jsx-max-props-per-line */
/* eslint-disable no-undef */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './AllVaccines.css'
import { useHistory } from "react-router-dom";
import {
  Avatar, Card, CardActions, CardContent, Checkbox,
  IconButton, Table, TableBody, TableCell, TableHead,
  TablePagination, TableRow, Typography
} from '@material-ui/core';
import { PeopleAlt, Delete } from '@material-ui/icons';
import { makeStyles } from '@material-ui/styles';
import clsx from 'clsx';
import { Types as VaccineTypes } from 'sagas/vaccine';


import PerfectScrollbar from 'react-perfect-scrollbar';

import { getInitials } from 'helpers';
import vaccine from 'store/vaccine';

const useStyles = makeStyles(theme => ({
  root: {},
  content: {
    padding: 0
  },
  inner: {
    minWidth: 1050
  },
  nameContainer: {
    display: 'flex',
    alignItems: 'center'
  },
  avatar: {
    marginRight: theme.spacing(2)
  },
  actions: {
    justifyContent: 'flex-end'
  }
}));


const AllVaccines = props => {
  const dispach = useDispatch();
  const history = useHistory();
  const vaccines = useSelector((state) => state.vaccines.data);
  const id = useSelector((state) => state.nurse.id);
  const classes = useStyles();
  const { className, selectedProjects, setSelectedProjects, ...rest } = props;
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [page, setPage] = useState(0);

  const projects = useSelector((state) => state.projects);


  const deleteVac = (vaccine) => {
    dispach({
      type: VaccineTypes.delete,
      payload: {
        idVacina: vaccine.idVacina,
         id: id,
        onSuccess: () => {
          history.push('/all-vaccines');
        }
      },
      
    })

  }

  const handleSelectAll = event => {
    let selectedProjects;

    if (event.target.checked) {
      selectedProjects = projects.map(project => project.id);
    } else {
      selectedProjects = [];
    }

    setSelectedProjects(selectedProjects);
  };

  const handleSelectOne = (event, id) => {
    const selectedIndex = selectedProjects.indexOf(id);
    let newSelectedProjects = [];

    if (selectedIndex === -1) {
      newSelectedProjects = newSelectedProjects.concat(selectedProjects, id);
    } else if (selectedIndex === 0) {
      newSelectedProjects = newSelectedProjects.concat(selectedProjects.slice(1));
    } else if (selectedIndex === selectedProjects.length - 1) {
      newSelectedProjects = newSelectedProjects.concat(selectedProjects.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelectedProjects = newSelectedProjects.concat(
        selectedProjects.slice(0, selectedIndex),
        selectedProjects.slice(selectedIndex + 1)
      );
    }

    setSelectedProjects(newSelectedProjects);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  return (
    <div className="all-vaccines">

      <Card

        className={clsx(classes.root, className)}
      >
        <CardContent className={classes.content}>
          <PerfectScrollbar>
            <div className={classes.inner}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell padding="checkbox">
                      {/* <Checkbox
                        checked={selectedProjects.length === vaccines.length}
                        color="primary"
                        indeterminate={
                          selectedProjects.length > 0 &&
                          selectedProjects.length < vaccines.length
                        }
                        onChange={handleSelectAll}
                      /> */}
                    </TableCell>
                    <TableCell>Nome</TableCell>
                    <TableCell />
                  </TableRow>
                </TableHead>
                <TableBody>
                  {vaccines.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((vaccine) => (
                    <TableRow
                      className={classes.tableRow}
                      hover
                      key={vaccine.id}
                    // selected={selectedProjects.indexOf(vaccine.id) !== -1}
                    >
                      {/* <TableCell padding="checkbox">
                        <Checkbox
                          checked={selectedProjects.indexOf(vaccine.id) !== -1}
                          color="primary"
                          onChange={event => handleSelectOne(event, vaccine.id)}
                          value="true"
                        />
                      </TableCell> */}
                      <TableCell>
                        <div className={classes.nameContainer}>
                          <Avatar
                            className={classes.avatar}
                            src={vaccine.avatarUrl}
                          >
                            {getInitials(vaccine.nome)}
                          </Avatar>
                          <Typography variant="body1">{vaccine.nome}</Typography>
                        </div>
                      </TableCell>
                      <TableCell>{vaccine.private}</TableCell>
                      <TableCell align="right">
                        <IconButton
                          aria-label="delete"
                          color="primary"
                          onClick={() => deleteVac(vaccine)}
                        >
                          <Delete />
                        </IconButton>

                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </div>
          </PerfectScrollbar>
        </CardContent>
        <CardActions className={classes.actions}>
          <TablePagination
            component="div"
            count={vaccines.length}
            labelRowsPerPage={<span>Linhas por página:</span>}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
            page={page}
            rowsPerPage={rowsPerPage}
            rowsPerPageOptions={[5, 10, 25]}
          />
        </CardActions>
      </Card>
    </div >
  );
};


export default AllVaccines;
