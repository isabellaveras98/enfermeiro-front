/* eslint-disable react/jsx-max-props-per-line */
/* eslint-disable no-undef */
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import {
  Grid,
  Button,
  TextField,
  CardContent,
  CardActions
} from '@material-ui/core';
import './FindPatient.css'
import sudv_logo1 from '../../images/sudv_logo1.png';

import { Types as PatientsTypes } from 'sagas/patient';

const schema = {
  cns: {
    presence: { allowEmpty: false, message: 'precisa ser preenchido' },
  },
};



const FindPatient = props => {
  const { history } = props;

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {},
  });

  const dispatch = useDispatch();
  const FindPatient = () => {
    dispatch({
      type: PatientsTypes.find,
      payload: {
        data: {
          cns: formState.values.cns,
        },
        onSuccess: () => {
          history.push('/patient');
        }
      }
    })
  }

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleFindPatient = event => {
    event.preventDefault();
    history.push('/');
  };

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <div className="find-patient">

      <img className="logo_sudv" src={sudv_logo1}/>

      <form
        onSubmit={handleFindPatient}

      >
        <CardContent>
          <Grid
            container
            spancing={3}
          >

            <TextField
              label="Nº CNS"
              className="input"
              error={hasError('cns')}
              inputProps={{ min: 0, style: { textAlign: 'center', borderRadius: '20px' } }}
              fullWidth
              helperText={
                hasError('cns') ? formState.errors.cns[0] : null
              }
              name="cns"
              onChange={handleChange}
              placeholder="Nº CNS"
              type="text"
              value={formState.values.cns || ''}
              variant="outlined"
            />
          </Grid>
        </CardContent>
        <CardActions>
          <Button
            color="primary"
            fullWidth
            onClick={FindPatient}
            size="large"
            type="submit"
            variant="contained"
          >
            Buscar
          </Button>
        </CardActions>

      </form>
    </div>
  );
};

FindPatient.propTypes = {
  history: PropTypes.object
};

export default withRouter(FindPatient);
