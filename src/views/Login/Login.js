/* eslint-disable react/jsx-max-props-per-line */
/* eslint-disable no-undef */
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import sudv_logo1 from '../../images/sudv_logo1.png';
import {
  Grid,
  Button,
  TextField,
  CardContent,
  CardActions,
  InputAdornment,
} from '@material-ui/core';
import './Login.css'

import { Types as NurseTypes } from 'sagas/nurse';
import { AccountCircle } from '@material-ui/icons';

const schema = {
  login: {
    presence: { allowEmpty: false, message: 'precisa ser preenchido' },
  },
};



const Login = props => {
  const { history } = props;

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {},
  });

  const dispatch = useDispatch();
  const login = () => {
    dispatch({
      type: NurseTypes.login,
      payload: {
        data: {
          login: formState.values.login,
          password: formState.values.password,
        },
        onSuccess: () => {
          history.push('/find-patient');
        }
      }
    })
  }

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleLogin = event => {
    event.preventDefault();
    history.push('/');
  };

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;


  return (
    <div className="login">

      <img className="logo_sudv" src={sudv_logo1}/>

      <form
        onSubmit={handleLogin}

      >
        <CardContent>
          <Grid
            container
            spancing={3}
          >

            <TextField
              className="input"
              error={hasError('e-mail')}
              fullWidth
              helperText={
                hasError('e-mail') ? formState.errors.login[0] : null
              }
              inputProps={{ min: 0, style: { textAlign: 'center', borderRadius: '20px' } }}
              label="E-mail"
              name="login"
              onChange={handleChange}
              placeholder="E-mail"
              startAdornment={
                <InputAdornment position="start">
                  <AccountCircle />
                </InputAdornment>
              }
              type="email"
              value={formState.values.login || ''}
              variant="outlined"
            />
          </Grid>
          <Grid
            container
            spancing={3}
          >

            <TextField
              className="input"
              error={hasError('password')}
              fullWidth
              helperText={
                hasError('password') ? formState.errors.password[0] : null
              }
              inputProps={{ min: 0, style: { textAlign: 'center', borderRadius: '20px' } }}
              label="Senha"
              name="password"
              onChange={handleChange}
              placeholder="Senha"
              type="password"
              value={formState.values.password || ''}
              variant="outlined"
            />
          </Grid>
        </CardContent>
        <CardActions>
          <Button
            color="primary"
            fullWidth
            onClick={login}
            size="large"
            type="submit"
            variant="contained"
          >
            Entrar
          </Button>
        </CardActions>

      </form>
    </div>
  );
};

Login.propTypes = {
  history: PropTypes.object
};

export default withRouter(Login);
