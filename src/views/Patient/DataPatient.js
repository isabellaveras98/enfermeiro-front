import { Card, CardContent, IconButton, Typography, CardActions } from '@material-ui/core';
import PropTypes from 'prop-types';
import React from 'react';
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { useSelector } from 'react-redux';
import { format, parseISO } from 'date-fns';

const DataPatient = props => {
  const {  back, onClickAddVaccination, } = props;
   
  const patient = useSelector((state) => state.patient.paciente);
  return (
    <Card className="data-patient">
      <CardContent>
        <Typography
          align="center"
          component="p"
          variant="h3"
        >
                    Nome: {patient.nome}
        </Typography>
        <Typography
          align="center"
          component="p"
          variant="h4"
        >
                    CNS: {patient.cns}
        </Typography>
        <Typography
          align="center"
          component="p"
          variant="h4"
        >
                    CPF: {patient.cpf}
        </Typography>
        <Typography
          align="center"
          component="p"
          variant="h4"
        >
                    Nascimento: {format(
            parseISO(patient.dtNascimento),
            'dd/MM/yyyy'
          )}
        </Typography>
        <CardActions disableSpacing >
          <IconButton 
            align="left"
            aria-label="add vaccine"
            className="btn-back"
            onClick={back}
          >
            <ArrowBackIcon />
            {/* <Typography
              align="center"
              variant="h4"
            /> */}
          </IconButton>

          <IconButton 
            aria-label="add vaccine"
            className="btn-vacinar"
            onClick={onClickAddVaccination}
          >
            <AddCircleRoundedIcon />
            <Typography
              align="right"
              variant="h4"
            >
                            Vacinar
            </Typography>
          </IconButton>
          
        </CardActions>
      </CardContent>
    </Card>
  )
}

DataPatient.propTypes = {
  onClickAddVaccination: PropTypes.func.isRequired,
}

export default DataPatient;