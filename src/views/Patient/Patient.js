import React from 'react';
import PropTypes from 'prop-types';
import DataPatient from './DataPatient';
import VaccinePatient from './VaccinesPatient';
import './Patient.css';
import { useDispatch } from 'react-redux';
import { Types as VaccinationTypes } from 'sagas/vaccination';
import { withRouter } from 'react-router';

const Patient = props => {
  const { history } = props;
  const dispatch = useDispatch();


  const onClickAddVaccination = () => {
    dispatch({
      type: VaccinationTypes.addVaccinationForm,
      payload: {
        onSuccess: () => {
          history.push('/vaccination/form')
        },
        onError: (error) => {
          console.error(error);
        }
      }
    });
  }

  const back = () => {
    history.push('/find-patient')
  }

  return (
    <div className="patient">
      <DataPatient back={back} onClickAddVaccination={onClickAddVaccination} />
      <VaccinePatient />
    </div>
  );
};

Patient.propTypes = {
  history: PropTypes.object
};

export default withRouter(Patient);
