import { Grid, Typography } from '@material-ui/core';
import { format, parseISO } from 'date-fns';
import React from 'react';
import { useSelector } from 'react-redux';
import { If } from 'helpers';

const VaccinesPatient = props => {
  const patient = useSelector((state) => state.patient);

  return (
    <div className="table-vaccines">
      <Grid
        container
        spacing={3}
      >
        <div className="info-content">
          <Grid spacing={3}>
            <If test={patient.paciente.isGravida === 'S'}>
              <p className="info-pregnant">* Gestante</p>
            </If>
          </Grid>
          
          <Grid spacing={3}>
            <If test={patient.paciente.doencas !== ''}>
              <p className="info-sickness">* {patient.paciente.doencas}</p>
            </If>
          </Grid>
        </div>
        
        {
          patient.vacinas.map((vaccine, index) => (

            <Grid
              item
              key={index}
              style={{ backgroundColor: vaccine.cor, color: '#ffffff' }}
            >
              <Typography
                align="center"
                className="vaccine"
                component="p"
                variant="h4"
              >
                {vaccine.nomeVacina}<br />
                                Data Validade: {format(
                  parseISO(vaccine.dtValidade),
                  'dd/MM/yyyy'
                )}<br />
                                Lote: {vaccine.lote}
              </Typography>
            </Grid>
          ))
        }
      </Grid>
    </div>
  )
}

export default VaccinesPatient;