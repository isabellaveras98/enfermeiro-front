import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { Grid, Typography } from '@material-ui/core';
import { useSelector } from 'react-redux';
import './Profile.css';


const Profile = props => {

  const nurse = useSelector((state) => state.nurse);

  return (
    <div className="profile">
      
      <Grid
        container
        justifyContent="left"
      >
        <Grid
          item
          sm={12}
        >
          <Typography
            className="nurse-info"
            variant="h4"
          ><b>Nome: </b>{nurse.nome}</Typography>
        </Grid>
        <Grid
          item
          sm={12}
        >
          <Typography variant="h4"><b>COREN: </b>{nurse.coren}</Typography>
        </Grid>

      </Grid>
    </div>
  );
};

Profile.propTypes = {
  history: PropTypes.object
};

export default withRouter(Profile);
