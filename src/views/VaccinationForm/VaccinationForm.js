/* eslint-disable react/jsx-max-props-per-line */
/* eslint-disable no-undef */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import {
  Grid,
  Button,
  TextField,
  CardContent,
  CardActions
} from '@material-ui/core';
import './VaccinationForm.css'
import { useHistory } from 'react-router-dom';
import sudv_logo1 from '../../images/sudv_logo1.png';
import { Types as VaccinationTypes } from 'sagas/vaccination';

const schema = {
  cns: {
    presence: { allowEmpty: false, message: 'precisa ser preenchido' },
  },
};



const VaccinationForm = props => {
  const history = useHistory();

  const cns = useSelector((state) => state.patient.paciente.cns);
  const id = useSelector((state) => state.nurse.id);


  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {},
  });

  const dispatch = useDispatch();

  const saveVaccination = () => {
    dispatch({
      type: VaccinationTypes.saveVaccination,
      payload: {
        data: {
          lote: formState.values.lote,
          nome: formState.values.nome,
          cns: cns,
          id: id,
        },
        onSuccess: () => {
          history.push('/patient');
        }
      }
    })
  }

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const handleVaccinationForm = event => {
    event.preventDefault();
    history.push('/');
  };

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <div className="vaccination-form">

      <img className="logo_sudv" src={sudv_logo1}/>

      <form
        onSubmit={handleVaccinationForm}

      >
        <CardContent>
          <Grid
            container
            spancing={3}
          >

            <TextField
              className="input"
              error={hasError('lote')}
              fullWidth
              helperText={
                hasError('lote') ? formState.errors.lote[0] : null
              }
              inputProps={{ min: 0, style: { textAlign: 'center', borderRadius: '20px' } }}
              label="Nº Lote"
              name="lote"
              onChange={handleChange}
              placeholder="Nº Lote"
              type="text"
              value={formState.values.lote || ''}
              variant="outlined"
            />
            <TextField
              className="input"
              error={hasError('nome')}
              fullWidth
              helperText={
                hasError('nome') ? formState.errors.nome[0] : null
              }
              inputProps={{ min: 0, style: { textAlign: 'center', borderRadius: '20px' } }}
              label="Nome da vacina"
              name="nome"
              onChange={handleChange}
              placeholder="Nome da vacina"
              type="text"
              value={formState.values.nome || ''}
              variant="outlined"
            />
          </Grid>
        </CardContent>
        <CardActions>
          <Button
            color="primary"
            fullWidth
            onClick={saveVaccination}
            size="large"
            type="submit"
            variant="contained"
          >
            Salvar
          </Button>
        </CardActions>

      </form>
    </div>
  );
};


export default VaccinationForm;
