/* eslint-disable react/jsx-max-props-per-line */
/* eslint-disable no-undef */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import validate from 'validate.js';
import {
  Grid,
  Button,
  TextField,
  Typography
} from '@material-ui/core';
import './VaccineNewAdd.css'
import { useHistory } from 'react-router-dom';

import { Types as VaccineTypes } from 'sagas/vaccine';
import InputColor from 'react-input-color';
import { DatePicker } from '@material-ui/pickers';

const schema = {
  cns: {
    presence: { allowEmpty: false, message: 'precisa ser preenchido' },
  },
};



const VaccineNewAdd = props => {
  const history = useHistory();

  const id = useSelector((state) => state.nurse.ubsId);
  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {},
  });

  const dispatch = useDispatch();

  const addVaccine = () => {
    dispatch({
      type: VaccineTypes.add,
      payload: {
        data: {
          lote: formState.values.lote,
          nome: formState.values.nome,
          nomeCampanha: formState.values.nomeCampanha,
          idUbs:id,
          dtValidade: data.toLocaleDateString(),
          cor: color,
          fornecedor: formState.values.fornecedor,
        },
        onSuccess: () => {
          history.push('/all-vaccines');
        }
      }
    })
  }
  const [data, setData] = React.useState(null);
  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState(formState => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {}
    }));
  }, [formState.values]);

  const handleChange = event => {
    event.persist();

    setFormState(formState => ({
      ...formState,
      values: {
        ...formState.values,
        [event.target.name]:
          event.target.type === 'checkbox'
            ? event.target.checked
            : event.target.value
      },
      touched: {
        ...formState.touched,
        [event.target.name]: true
      }
    }));
  };

  const [initial, setInitial] = useState('#fff');
  const [color, setColor] = useState({});

  const handleVaccineAdd = event => {
    event.preventDefault();
    history.push('/find-patient');
  };

  const hasError = field =>
    formState.touched[field] && formState.errors[field] ? true : false;

  return (
    <div className="add-vaccine">
      
      <form
        onSubmit={handleVaccineAdd}

      >
        <Grid
          container
          justifyContent="center"
          spancing={3}
          style={{
            backgroundColor: '#c0f8ffb3',
            paddingLeft: 50,
            paddingRight: 50,
            paddingBottom: 50,
            paddingTop: 50,
            borderRadius: 15
          }}
        >
          {/* <Grid item sx={12}>
            <Typography align="center" variant="h4">
              Informe os dados da vacina
            </Typography>
          </Grid> */}
          <Grid item md={6} sm={12}>
            <TextField
              error={hasError('lote')}
              fullWidth
              helperText={
                hasError('lote') ? formState.errors.lote[0] : null
              }
              inputProps={{ min: 0, style: { textAlign: 'center', borderRadius: '20px' } }}
              label="Nº Lote"
              name="lote"
              onChange={handleChange}
              placeholder="Nº Lote"
              type="text"
              value={formState.values.lote || ''}
            />
          </Grid>

          <Grid item md={6} sm={12}>
            <TextField
              error={hasError('nome')}
              fullWidth
              helperText={
                hasError('nome') ? formState.errors.nome[0] : null
              }
              inputProps={{ min: 0, style: { textAlign: 'center', borderRadius: '20px' } }}
              label="Nome da vacina"
              name="nome"
              onChange={handleChange}
              type="text"
              value={formState.values.nome || ''}
            />
          </Grid>

          <Grid item md={6} sm={12}>
            <TextField
              error={hasError('nomeCampanha')}
              fullWidth
              helperText={
                hasError('nomeCampanha') ? formState.errors.nomeCampanha[0] : null
              }
              inputProps={{ min: 0, style: { textAlign: 'center', borderRadius: '20px' } }}
              label="Nome da campanha"
              name="nomeCampanha"
              onChange={handleChange}
              type="text"
              value={formState.values.nomeCampanha || ''}
            />
          </Grid>

          <Grid item md={6} sm={12}>
            <TextField
              error={hasError('fornecedor')}
              fullWidth
              helperText={
                hasError('fornecedor') ? formState.errors.fornecedor[0] : null
              }
              inputProps={{ min: 0, style: { textAlign: 'center', borderRadius: '20px' } }}
              label="Fornecedor"
              name="fornecedor"
              onChange={handleChange}
              type="text"
              value={formState.values.fornecedor || ''}
            />
          </Grid>

          <Grid item md={6} sm={12}>
            <DatePicker
              format="d MMM yyyy"
              label="Data Validade"
              onChange={(newValue) => {
                setData(newValue);
              }}
              value={data}
              
            />
          </Grid>
          <br />

          <Grid item md={6} sm={12}>
            <Typography className="selo" display="block" style={{ marginTop: 15 }} variant="overline">
              Cor do selo
              <InputColor initialValue={initial} onChange={setColor} style={{ marginLeft: 15 }} />
            </Typography>

          </Grid>

          <Grid>
            <Button
              className="btn-add-vaccine"
              color="primary"
              fullWidth
              onClick={addVaccine}
              size="large"
              style={{ marginLeft: 0 }}
              type="submit"
              variant="contained"
            >
              Adicionar
            </Button>
          </Grid>
        </Grid>
      </form>
    </div >
  );
};


export default VaccineNewAdd;
